function onLoad() {
    u_addPlaceholderAttribute('user','Someones name goes here');
}
function u_addPlaceholderAttribute(variableName, hint) {
    try {
        var fieldName = g_form.getControl(variableName).name.toString();
        if (Prototype.Browser.IE) {
            fieldName.placeholder = hint;
        } else {
            $(fieldName).writeAttribute('placeholder', hint);
        }
    } catch (err) {}
}